package com.example.cc3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.min

class MainActivity : AppCompatActivity() {

    private var firstNumber = 0.0
    private var secondNumber = 0.0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        one.setOnClickListener {
            writeNumber(one)
        }
        two.setOnClickListener {
            writeNumber(two)
        }
        three.setOnClickListener {
            writeNumber(three)
        }
        four.setOnClickListener {
            writeNumber(four)
        }
        five.setOnClickListener {
            writeNumber(five)
        }
        six.setOnClickListener {
            writeNumber(six)
        }
        seven.setOnClickListener {
            writeNumber(seven)
        }
        eight.setOnClickListener {
            writeNumber(eight)
        }
        nine.setOnClickListener {
            writeNumber(nine)
        }
        zero.setOnClickListener {
            writeNumber(zero)
        }

        equal.setOnClickListener {
            equal()
        }
        dot.setOnClickListener{
            writeNumber(dot)
        }
        minus.setOnClickListener {
            minusFunction(minus)
        }
        plus.setOnClickListener {
            addOperation(plus)
        }
        split.setOnClickListener {
            addOperation(split)
        }
        multiply.setOnClickListener {
            addOperation(multiply)
        }
        deleteAll.setOnClickListener {
            deleteAll()
        }
        textResult.setOnClickListener {

        }
        backspace.setOnClickListener {
            delete()
        }
        backspace.setOnLongClickListener{
            firstNumber = 0.0
            secondNumber = 0.0
            textResult.text = ""
            true
        }

    }


    private fun writeNumber(button: Button) {
        textResult.text = textResult.text.toString() + button.text.toString()
    }

    private fun deleteAll() {
        textResult.text = ""
        firstNumber = 0.0
        secondNumber = 0.0
    }

    private fun delete() {
        var text = textResult.text.toString()
        if (text.isNotEmpty())
            text = text.substring(0, text.length - 1)
        textResult.text = text
    }

    private fun addOperation(button: Button) {
        if (textResult.text.isNotEmpty()) {
            operation = button.text.toString()
            firstNumber = textResult.text.toString().toDouble()
            textResult.text = ""
        }
    }

    private fun equal() {
        if (textResult.text.isNotEmpty()) {
            secondNumber = textResult.text.toString().toDouble()
            var result = 0.0
            if (operation == "+") {
                result = firstNumber + secondNumber
            } else if (operation == "-") {
                result = firstNumber - secondNumber
            } else if (operation == "*") {
                result = firstNumber * secondNumber
            } else if (operation == "/") {
                result = firstNumber / secondNumber
            }
            textResult.text = result.toString()
            firstNumber = 0.0
            secondNumber = 0.0
        }
    }
    private fun minusFunction(button: Button){
        if (textResult.text.isEmpty()){
            textResult.text = minus.text.toString()
        }else {
            operation = button.text.toString()
            firstNumber = textResult.text.toString().toDouble()
            textResult.text = ""
        }
    }


}






